##
## Makefile for my_ftl in /home/jordan/rendu/my_ftl/verove_j/my_FTL
## 
## Made by VEROVE Jordan
## Login   <verove_j@etna-alternance.net>
## 
## Started on  Mon Nov  7 11:53:12 2016 VEROVE Jordan
## Last update Wed Oct 16 09:51:41 2019 VEROVE Jordan
##

SRC_DIR =	srcs/

CC =		gcc

CFLAGS =	-Wall -Werror -Wextra

RM =		rm -rf

SRC =		$(SRC_DIR)get_inputs.c \
		$(SRC_DIR)roulette.c \
		$(SRC_DIR)roulette_print.c \
		$(SRC_DIR)tools.c \
		$(SRC_DIR)tools2.c \
		$(SRC_DIR)main.c

OBJ =		$(SRC:.c=.o)

NAME =		roulette

$(NAME):	$(OBJ)
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME)

all:		$(NAME)

clean:
		$(RM) $(OBJ)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
