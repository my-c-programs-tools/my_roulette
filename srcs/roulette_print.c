/*
** roulette_print.c for la roulette in /home/jordan/la roulette/verove_j/roulette
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Sat Oct 22 09:42:36 2016 VEROVE Jordan
** Last update Sat Oct 22 12:06:32 2016 VEROVE Jordan
*/

void	my_putnbr(int nb);
void	my_putchar(char c);
void	my_putstr(char *str);

void	print_asks(int nb)
{
  if (nb == 0)
    my_putstr("--> Quel est le montant de votre mise ?\n");
  else if (nb == 1)
    my_putstr("--> Sur quel chiffre misez-vous ? (1 à 36)\n");
  else
    my_putstr("Voulez vous continuer ? (y,n)\n");
}

void	print_gain(int gain)
{
  my_putstr("Votre gain actuel est de : ");
  my_putnbr(gain);
  my_putstr(" €\n");
}

void	print_result(int gain, int rand_value, int up_value, int state)
{
  my_putstr("Les jeux sont fait...\nLe résultat est : ");
  my_putnbr(rand_value);
  my_putchar('\n');
  if (state == 0)
    {
      my_putstr("Félicitation! Votre mise est exacte.\
 Vos gains augmentent de ");
      my_putnbr(up_value);
    }
  else if (state == 1)
    {
      my_putstr("Votre mise est de la même couleur. Vos gains augmentent de ");
      my_putnbr(up_value);
    }
  else
    {
      my_putstr("Vous n'avez rien gagné. Vous perdez donc votre mise.");
    }
  my_putstr("\nVotre gain actuel est de : ");
  my_putnbr(gain);
  my_putstr(" €\n");
}
