/*
** tools2.c for la roulette in /home/jordan/la roulette/verove_j/roulette
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Sat Oct 22 10:14:23 2016 VEROVE Jordan
** Last update Sat Oct 22 12:55:30 2016 VEROVE Jordan
*/

void	my_putstr(char *str);
void	my_putnbr(int nb);
void	should_quit();

int	my_get_mise_nbr(char *str)
{
  int	nb;
  int	i;

  i = 0;
  nb = 0;
  while (str[i] != 0)
    i++;
  if (i == 2 && ((str[0] <= '9' && str[0] >= '0')
		 && (str[1] <= '9' && str[1] >= '0')))
    {
      return (nb += (((str[0] - 48) * 10) + (str[1] - 48)));
    }
  if (i == 1 && (str[0] <= '9' && str[0] >= '0'))
    {
      return (nb += (str[0] - 48));
    }
  else if (i == 3 && ((str[0] <= '9' && str[0] >= '0')
		      && (str[1] <= '9' && str[1] >= '0')
		      && (str[2] <= '9' && str[2] >= '0')))
    {
      return (nb += (((str[0] - 48) * 100) + (str[1] - 48) * 10) + (str[2] - 48));
    }
  return (-1);
}

int	my_get_chiffre_nbr(char *str)
{
  int	nb;
  int	i;

  i = 0;
  nb = 0;
  while (str[i] != 0)
    i++;
  if (i == 0 || i > 2)
    return (-1);
  if (str[1] != 0 && ((str[0] <= '9' && str[0] >= '0')
		      || (str[1] <= '9' && str[1] >= '0')))
    {
      nb += ((str[0] - 48) * 10);
      nb += (str[1] - 48);
      return (nb);
    }
  else if (str[1] == 0  && (str[0] <= '9' && str[0] >= '0'))
    {
      nb += (str[0] - 48);
      return (nb);
    }
  else
    return (-1);
}
