/*
** roulette.c for la roulette in /home/jordan/la roulette/verove_j/roulette
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Sat Oct 22 09:36:05 2016 VEROVE Jordan
** Last update Wed Oct 16 10:00:31 2019 VEROVE Jordan
*/

#include <stdbool.h>
#include <time.h>
#include <stdlib.h>

void	print_asks(int nb);
void	print_gain(int gain);
char	*readLine();
void	my_putnbr(int nb);
void	my_putstr(char *str);
int	get_chiffre();
int	should_quit();
void	print_result(int gain, int rand_value, int up_value, int state);
int	check_mise(int gain);
int	check_chiffre();

int	get_rand_value()
{
  int	random;

  srand(time(NULL));
  random = (rand() % 36) + 1;
  return (random);
}

int	check_value(int gain, int mise, int rand_value, int chiffre)
{
  if (chiffre == rand_value)
    {
      gain += (mise * 3);
      print_result(gain, rand_value, (mise * 3), 0);
      return (gain);
    }
  else if (((((chiffre % 2) == 0) && ((rand_value % 2) == 0)))
	   || (((chiffre % 2) == 1) && ((rand_value % 2) == 1)))
    {
      gain += (mise / 2);
      print_result(gain, rand_value, (mise / 2), 1);
      return (gain);
    }
  else
    {
      gain -= mise;
      print_result(gain, rand_value, mise, 2);
      return (gain);
    }
}

void	roulette_loop()
{
  int	gain;
  int	mise;
  int	chiffre;
  int	random;
  bool	quit;

  gain = 200;
  quit = false;
  my_putstr("\e[1;1H\e[2J");
  my_putstr("Bienvenu à la Roulette ETNA\nVotre solde actuel est de : 200 €\n");
  while (gain > 0 && quit == false)
    {
      mise = check_mise(gain);
      chiffre = check_chiffre();
      random = get_rand_value();
      gain = check_value(gain, mise, random, chiffre);
      if (should_quit() == 0)
	quit = true;
    }
  my_putstr("Merci de votre participation à la roulette ETNA!\n");
}
