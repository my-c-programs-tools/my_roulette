/*
** get_inputs.c for la roulette in /home/jordan/la roulette/verove_j/roulette
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Sat Oct 22 10:11:06 2016 VEROVE Jordan
** Last update Wed Oct 16 10:00:23 2019 VEROVE Jordan
*/

char	*readLine();
void	my_putstr();
int	my_strlen(char *str);
void	my_putchar(char c);
int	my_get_mise_nbr(char *str);
int	my_get_chiffre_nbr(char *str);

int	get_mise(int gain)
{
  char	*answere;
  int	mise;

  my_putstr("--> Quel est le montant de votre mise ?\n");
  answere = readLine();
  my_putchar('\n');
  mise = my_get_mise_nbr(answere);
  if (mise == -1) {
    my_putstr("Error : votre mise est inférieure à 0 ou ne correspond pas à un nombre.\n");
  }
  else if (mise || mise > gain) {
    my_putstr("Error : votre mise est supérieure à votre solde.\n");
  }
  return (mise);
}

int	check_mise(int gain)
{
  int	mise;

  mise = get_mise(gain);
  if (mise == -1 || mise > gain)
    {
      while (mise == -1 || mise > gain)
	mise = get_mise(gain);
    }
  return (mise);
}

int	get_chiffre()
{
  char	*answere;
  int	chiffre;

  my_putstr("--> Sur quel nombre misez-vous ? (1 à 36)\n");
  answere = readLine();
  my_putchar('\n');
  chiffre = my_get_chiffre_nbr(answere);
  if (chiffre > 36)
    my_putstr("Error : vous devez miser entre 1 et 36.\n");
  return (chiffre);
}

int	check_chiffre()
{
  int	chiffre;

  chiffre = get_chiffre();
  if (chiffre > 36)
    {
      while (chiffre > 36)
	chiffre = get_chiffre();
    }
  return (chiffre);
}

int	should_quit()
{
  int	answere_size;
  char	*answere;

  my_putstr("Voulez-vous continuer ?(y,n)\n");
  answere = readLine();
  answere_size = my_strlen(answere);
  if (answere_size != 1 || (answere_size == 1 && answere[0] != 'y'
			    && answere[0] != 'n'))
    {
      my_putstr("Répondre par y pour oui et n pour non.\n");
      should_quit();
      return (-1);
    }
  if (answere_size == 1 && (answere[0] == 'y')) {
    my_putstr("\e[1;1H\e[2J");
    return (1);
  }
  else if (answere_size == 1 && (answere[0] == 'n'))
    return (0);
  return (0);
}
